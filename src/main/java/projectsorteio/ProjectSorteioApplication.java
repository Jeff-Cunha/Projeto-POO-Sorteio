package projectsorteio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSorteioApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectSorteioApplication.class, args);
	}

}
