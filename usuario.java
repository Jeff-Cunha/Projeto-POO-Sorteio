import java.util.Date;
import java.util.List;

public class usuario {

    private int id;
    private boolean consentimento;
    private Date dataNascimento;
    private String nome;
    private String cpf;
    private String email;
    private String perfil;
    private String role;
    private String senha;
    private String telefone;
    private String estado;
    private String municipio;
    private String cep;
    private String bairro;
    private String complemento;

    private List<bilhete> bilhetes;

    private List<nota_fiscal> nota_iscais;

}